<?php

$courseId = isset($_GET['courseID']) ? $_GET['courseID'] : "" ;
$courseId = isset($_POST['courseId']) ? $_POST['courseId'] : $courseId ;
$payment = isset($_GET['payment']) ? $_GET['payment'] : "" ;
$honorific = isset($_POST['honorific']) ? $_POST['honorific'] : "" ;
$name = isset($_POST['name']) ? $_POST['name'] : "" ;
$surname = isset($_POST['name']) ? $_POST['surname'] : "" ;
$occupation = isset($_POST['occupation']) ? $_POST['occupation'] : "" ;
$organisation = isset($_POST['organisation']) ? $_POST['organisation'] : "" ;
$department = isset($_POST['department']) ? $_POST['department'] : "" ;
$address = isset($_POST['address']) ? $_POST['address'] : "" ;
$town = isset($_POST['town']) ? $_POST['town'] : "" ;
$postCode = isset($_POST['postCode']) ? $_POST['postCode'] : "" ;
$country = isset($_POST['country']) ? $_POST['country'] : "" ;
$phone = isset($_POST['phone']) ? $_POST['phone'] : "" ;
$email = isset($_POST['email']) ? $_POST['email'] : "" ;
$disabilityRequirement = isset($_POST['disabilityRequirement']) ? "1" : "0" ;
$disabilityDetail = isset($_POST['disabilityDetail']) ? $_POST['disabilityDetail'] : "" ;
$dietaryRequirement = isset($_POST['dietaryRequirement']) ? "1" : "0" ;
$dietaryDetail = isset($_POST['dietaryDetail']) ? $_POST['dietaryDetail'] : "" ;
$cost = isset($_POST['cost']) ? $_POST['cost'] : "" ;
$agreesToMarketing = isset($_POST['agreesToMarketing']) ? "1" : "0" ;
$interest = isset($_POST['interest']) ? $_POST['interest'] : "" ;
$heardAboutUs = isset($_POST['heardAboutUs']) ? $_POST['heardAboutUs'] : "" ;
$heardAboutUsSQL = isset($_POST['heardAboutUsSQL']) ? $_POST['heardAboutUsSQL'] : "" ;
$heardAboutUsOther = isset($_POST['heardAboutUsOther']) ? $_POST['heardAboutUsOther'] : "" ;
$dob = isset($_POST['dob']) ? $_POST['dob'] : "" ;

$now = new dateTime(null, new DateTimezone("Europe/London"));
$datetime = $now->format('Y-m-d H:i:s');
$encryption=md5($datetime);
$errors = "";

//what stage are we at?
//1 = No course ID
//2 = In progress
//3 = completed

//default value
$stage = 1;

//if you came via online paments
if ($payment) {
    $stage = 3;
}

//we have course details
if ($courseId) {

    //connect to DB
    require_once "fst/cpd/mysql_edit_pdo.php";

    //get course info
    $sql = "SELECT * FROM maths_courses WHERE courseID= :courseId ORDER BY start_date DESC";
    $statement = $db->prepare($sql);
    $statement->execute(array(':courseId' => $courseId));
    $course = $statement->fetch(PDO::FETCH_ASSOC);

    if ($course) {

        $stage = 2; //In progress

        //handle dates
        $startDate = new dateTime($course['start_date'] . $course['start_time'], new DateTimezone("Europe/London"));
        $endDate = new dateTime($course['end_date'] . $course['end_time'], new DateTimezone("Europe/London"));
        $deadline = new dateTime($course['registration_deadline'], new DateTimezone("Europe/London"));
        $m1 = $startDate->format('m');
        $m2 = $endDate->format('m');
        if ($m1 != $m2) {
            $startDateStr = $startDate->format('jS M');
        } else {
            $startDateStr = $startDate->format('jS ');
        }
        $endDateStr = $endDate->format('jS M Y');
        if ($startDate != $endDate) {
            $dateStr = $startDateStr . " - " . $endDateStr;
        } else {
            $dateStr = $endDateStr;
        }

        //get destination link
        $sql="SELECT link FROM maths_online_store WHERE onlineID = :bookingLink";
        $statement = $db->prepare($sql);
        $statement->execute(array(':bookingLink' => $course['booking_link']));
        $destination = $statement->fetch(PDO::FETCH_ASSOC);

        //get cost information
        $sql="SELECT detail FROM maths_costs WHERE costID= :cost LIMIT 1";
        $statement = $db->prepare($sql);
        $statement->execute(array(':cost' => $cost));
        $fundingDetail = $statement->fetch(PDO::FETCH_ASSOC);

        //get costs associated with this course
        $sql="SELECT maths_courses.title, maths_costs.costID, maths_costs.detail, maths_costs.day1, maths_costs.day2, maths_costs.day3, maths_costs.special, maths_courses_have_costs.length FROM maths_courses INNER JOIN (maths_costs INNER JOIN maths_courses_have_costs ON maths_costs.costID = maths_courses_have_costs.costID) ON maths_courses.courseID = maths_courses_have_costs.courseID WHERE maths_courses.courseID = :courseId";
        $statement = $db->prepare($sql);
        $statement->execute(array(':courseId' => $courseId));
        $costs = $statement->fetchAll();

        //get list of countries
        $sql="SELECT country FROM maths_countries WHERE country <> '' ORDER BY country ASC";
        $statement = $db->prepare($sql);
        $statement->execute();
        $countryList = $statement->fetchAll();

    }
}

//form has been submitted
if (!empty($_POST['courseId'])) {

    if (!$honorific) {
        $errors .= "Please select a title<br>\n";
    }
    if (!$name) {
        $errors .= "Please enter your first name<br>\n";
    }
    if (!$surname) {
        $errors .= "Please enter your surname<br>\n";
    }
    if (!$occupation) {
        $errors .= "Please enter your occupation / status<br>\n";
    }
    if (!$organisation) {
        $errors .= "Please enter the name of your organisation / academic institution<br>\n";
    }
    if (!$department) {
        $errors .= "Please enter your department within your organisation<br>\n";
    }
    if (!$address) {
        $errors .= "Please enter the first section of your address<br>\n";
    }
    if (!$town) {
        $errors .= "Please enter your town<br>\n";
    }
    if (!$postCode) {
        $errors .= "Please enter your post code<br>\n";
    }
    if (!$country) {
        $errors .= "Please select a country<br>\n";
    }
    if (!$phone) {
        $errors .= "Please enter a contact number<br>\n";
    }
    if (!$dob) {
        $errors .= "Please provide your date of birth<br>\n";
    }
    if (!$email) {
        $errors .= "Please enter your email address<br>\n";
    }
    $emailRegEx = "/^\w(\.?[\w-])*@\w(\.?[\w-])*\.[a-z]{2,6}(\.[a-z]{2})?$/i";
    if ($email and !preg_match($emailRegEx, $email)) {
        $errors .= "Please double check your email address<br>\n";
    }
    if (!$cost) {
        $errors .= "Please select a payment option<br>\n";
    }

    if (!$errors) {

        $sql="INSERT INTO maths_users (
            courseID,
            name_title,
            name,
            surname,
            occupation,
            organisation,
            department,
            address,
            town,
            postCode,
            country,
            phone,
            email,
            disabilityRequirement,
            disabilityDetail,
            dietaryRequirement,
            dietaryDetail,
            cost,
            agreesToMarketing,
            interest,
            heardAboutUs,
            heardAboutUsOther,
            dob,
            encryption,
            datetime
        ) VALUES (
            :courseId,
            :honorific,
            :name,
            :surname,
            :occupation,
            :organisation,
            :department,
            :address,
            :town,
            :postCode,
            :country,
            :phone,
            :email,
            :disabilityRequirement,
            :disabilityDetail,
            :dietaryRequirement,
            :dietaryDetail,
            :cost,
            :agreesToMarketing,
            :interest,
            :heardAboutUs,
            :heardAboutUsOther,
            :dob,
            :encryption,
            :datetime
        )";
        
        $statement = $db->prepare($sql);
        
        $statement->bindParam(':courseId', $courseId, PDO::PARAM_INT);
        $statement->bindParam(':honorific', $honorific, PDO::PARAM_STR);
        $statement->bindParam(':name', $name, PDO::PARAM_STR);
        $statement->bindParam(':surname', $surname, PDO::PARAM_STR);
        $statement->bindParam(':occupation', $occupation, PDO::PARAM_STR);
        $statement->bindParam(':organisation', $organisation, PDO::PARAM_STR);
        $statement->bindParam(':department', $department, PDO::PARAM_STR);
        $statement->bindParam(':address', $address, PDO::PARAM_STR);
        $statement->bindParam(':town', $town, PDO::PARAM_STR);
        $statement->bindParam(':postCode', $postCode, PDO::PARAM_STR);
        $statement->bindParam(':country', $country, PDO::PARAM_STR);
        $statement->bindParam(':phone', $phone, PDO::PARAM_STR);
        $statement->bindParam(':email', $email, PDO::PARAM_STR);
        $statement->bindParam(':disabilityRequirement', $disabilityRequirement, PDO::PARAM_STR);
        $statement->bindParam(':disabilityDetail', $disabilityDetail, PDO::PARAM_STR);
        $statement->bindParam(':dietaryRequirement', $dietaryRequirement, PDO::PARAM_STR);
        $statement->bindParam(':dietaryDetail', $dietaryDetail, PDO::PARAM_STR);
        $statement->bindParam(':cost', $cost, PDO::PARAM_INT);
        $statement->bindParam(':agreesToMarketing', $agreesToMarketing, PDO::PARAM_INT);
        $statement->bindParam(':interest', $interest, PDO::PARAM_STR);
        $statement->bindParam(':heardAboutUs', $heardAboutUs, PDO::PARAM_STR);
        $statement->bindParam(':heardAboutUsOther', $heardAboutUsOther, PDO::PARAM_STR);
        $statement->bindParam(':dob', $dob, PDO::PARAM_STR);
        $statement->bindParam(':encryption', $encryption, PDO::PARAM_STR);
        $statement->bindParam(':datetime', $datetime, PDO::PARAM_STR);
        
        $statement->execute();

        //email values
        $emailPlaceholders = array('{{name}}', '{{courseTitle}}', '{{dateStr}}', '{{duration}}', '{{fundingDetail}}', '{{destination}}');
        $emailValues = array(htmlentities($name), $course['title'], $dateStr, $course['duration'], $fundingDetail['detail'], $destination['link']);

        //email text
        $plainTextEmailContent = <<<'EOT'
Dear {{name}},

Thank you for registering for {{courseTitle}} ({{dateStr}}).

Our preferred method of payment is with credit or debit card via the Lancaster University Online Store.

Please follow the link below and select:

{{duration}} x {{fundingDetail}}

{{destination}}

Alternatively please contact psc@lancaster.ac.uk if you have any enquiries regarding payments.

We will be in touch again before the course.

Kind regards,
The Short Courses Team
EOT;

        $htmlEmailContent = <<<'HTML'
<html>
<body>
<p>Dear {{name}}</p>
<p> Thank you for registering for {{courseTitle}} ({{dateStr}}).</p>
<p>Our preferred method of payment is via the <a href="{{destination}}">Lancaster University Online store</a>.</p>
<p>Please select <strong>{{duration}} x {{fundingDetail}}</strong>.</p>
<p>Alternatively please contact <a href="mailto:psc@lancaster.ac.uk">psc@lancaster.ac.uk</a> if you have any enquiries regarding payments.</p>
<p>We will be in touch again before the course.</p>
<p>
Kind regards,<br>
The Short Courses Team<br>
</p>
</body>
</html>
HTML;

        //do exceptions for FMC
        if ($course['fmc'] === '1') {

            $plainTextEmailContent = <<<'EOT'
Dear {{name}},

If you wish to be invoiced for your fee, contact psc@lancaster.ac.uk regarding payment.

If you wish to pay by credit card, visit the Lancaster University Online Store using the link below and select:

{{duration}} x {{fundingDetail}}

{{destination}}

We will be in touch again before the course.
Kind regards,
The Short Courses Team
EOT;

            $htmlEmailContent = <<<'HTML'
<html>
<body>
<p>Dear {{name}},</p>
<p> Thank you for registering for {{courseTitle}} ({{dateStr}}).</p>
<p>If you wish to be invoiced for your fee, contact psc@lancaster.ac.uk regarding payment.</p>
<p>If you wish to pay by credit card, visit the <a href="{{destination}}">Lancaster University Online store</a>.</p>
<p>Please select <strong>{{duration}} x {{fundingDetail}}</strong>.</p>
<p>We will be in touch again before the course.</p>
<p>
Kind regards,<br>
The Short Courses Team<br>
</p>
</body>
</html>
HTML;

        } elseif ($course['sew'] === '1') {

            $plainTextEmailContent = <<<'EOT'
Dear {{name}},

Thank you for registering for {{courseTitle}} ({{dateStr}}).

We will be in touch again before the course.

Kind regards,
The Short Courses Team";
EOT;

            $htmlEmailContent = <<<'HTML'
<html>
<body>
<p>Dear {{name}},</p>
<p>Thank you for registering for {{courseTitle}} ({{dateStr}}).</p>
<p>We will be in touch again before the course.</p>
<p>Kind regards,<br>
The Short Courses Team<br>
</p>
</body>
</html>
HTML;

        }

        //email customer
        $to = "{$name} {$surname} <{$email}>";

        $subject = "Registration confirmation";

        $boundary = md5(uniqid(rand()));

        $header = "MIME-Version: 1.0\r\n";
        $header .= "From: Short Courses in Statistics <psc@lancaster.ac.uk>\r\n";
        $header .= "Content-Type: multipart/alternative;boundary={$boundary}\r\n";
        //$header .= "BCC: j.smith8@lancaster.ac.uk\r\n";

        $body = "This is a MIME encoded message.";
        $body .= "\r\n\r\n--" . $boundary . "\r\n";
        $body .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
        $body .= str_replace($emailPlaceholders, $emailValues, $plainTextEmailContent);
        $body .= "\r\n\r\n--" . $boundary . "\r\n";
        $body .= "Content-type: text/html;charset=utf-8\r\n\r\n";
        $body .= str_replace($emailPlaceholders, $emailValues, $htmlEmailContent);
        $body .= "\r\n\r\n--" . $boundary . "--";

        @mail($to, $subject, $body, $header);

        //Email the PSC team
        $to = "psc@lancaster.ac.uk";
        
        $subject = "{$course['title']} - {$name}";

        $header = "From: Short Courses Website <psc@lancaster.ac.uk>\r\n";
        $header .= "Content-Type: text/plain;\r\n";
        //$header .= "BCC: j.smith8@lancaster.ac.uk\r\n";

        $body = "There has been a new registration:\r\n";
        $body .= "Course: {$course['title']} ({$dateStr})\r\n";
        $body .= "Name: {$name} {$surname}\r\n";
        $body .= "Occupation: {$occupation}\r\n";
        $body .= "Organisation: {$organisation}\r\n";
        $body .= "Department: {$department}\r\n";
        $body .= "Address: {$address}, {$town}, {$postCode}, {$country}\r\n";
        $body .= "Phone: {$phone}\r\n";
        $body .= "Email: {$email}\r\n";

        mail($to, $subject, $body, $header);

        $stage = 3; //completed
        $payment = "regConfirmed";

    //end error check
    }

//end post check
}

//include "fst/cpd_dev/header.php";

//not started
if ($stage === 1):
    ?>

    <h1>Registration</h1>
    <p>To register onto a course please follow the <strong>Book now</strong> link at the top of each <a href="../list-of-courses/">course page</a>.</p>

    <?php
//end stage 1
endif;

//In progress
if ($stage === 2):
?>

<h1>Registration</h1>
<p>You are registering for: <strong><?= $course['title'] ?></strong> (<?= $dateStr ?>)</p>

<form name="registration" method="post" action="<?= htmlentities($_SERVER['PHP_SELF']) ?>">

<div class="eform non-modal-form">

<?php
//display error messages
if ($errors):
?>

<p class="form-error">
<?= $errors ?>
</p>

<?php
//end errors
endif;
?>

<p><span class="required-star">*</span> = required information</p>

<fieldset>
<legend><span class="t22px">Personal Information</span></legend>

<dl>

<dt class="dropdown">
<label for="honorific">
<span>Title</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<select name="honorific" id="honorific" class="dropdown">
<option value=""></option>
<?php
$titles = array('Mr', 'Miss', 'Mrs', 'Ms', 'Dr', 'Professor');
$html = "";
foreach ($titles as $value) {
    $html .= "<option value=\"" . $value ."\"";
    if ($honorific === $value) {
        $html .= " selected='selected'";
    }
    $html .= ">" . $value . "</option>\n";
}
echo $html;
?>
</select>
</dd>

<dt class="textbox">
<label for="name">
<span>Forenames</span>
<span class="required-star">*</span>
</dt>
<dd>
<input type="text" id="name" name="name" value="<?= htmlentities($name) ?>">
</dd>

<dt class="textbox">
<label for="surname">
<span>Surname</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="text" id="surname" name="surname" value="<?= htmlentities($surname) ?>">
</dd>

<dt class="textbox">
<label for="dob">
<span>Date of birth</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="text" placeholder="DD / MM / YYYY" id="dob" name="dob" value="<?= htmlentities($dob) ?>">
</dd>

<dt class="textbox">
<label for="occupation">
<span>Occupation / Status</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="text" id="occupation" name="occupation" value="<?= htmlentities($occupation) ?>">
</dd>

<dt class="textbox">
<label for="organisation">
<span>Organisation / Academic Institution</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="text" id="organisation" name="organisation" value="<?= htmlentities($organisation) ?>">
</dd>

<dt class="textbox">
<label for="department">
<span>Department</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="text" id="department" name="department" value="<?= htmlentities($department) ?>">
</dd>

</dl>

</fieldset>

<fieldset>
<legend><span class="t22px">Contact Information</span></legend>

<dl>

<dt class="textbox">
<label for="address">
<span>Address</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<textarea id="address" name="address"><?= htmlentities($address) ?></textarea>
</dd>

<dt class="textbox">
<label for="town">
<span>Town</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="text" id="town" name="town" value="<?= htmlentities($town) ?>">
</dd>

<dt class="textbox">
<label for="postCode">
<span>Post Code</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input id="postCode" name="postCode" value="<?= htmlentities($postCode) ?>">
</dd>

<dt class="dropdown">
<label for="country">
<span>Country</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<select name="country" id="country">
<option value=""></option>
<option value="United Kingdom">United Kingdom</option>
<?php
$html = "";
foreach ($countryList as $value) {
    $html .= "<option value=\"" . $value['country'] ."\"";
    if ($country === $value['country']) {
        $html .= " selected='selected'";
    }
    $html .= ">" . $value['country'] . "</option>\n";
}
echo $html;
?>
</select>
</dd>

<dt>
<label for="phone">
<span>Phone</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="text" id="phone" name="phone" value="<?= htmlentities($phone) ?>">
</dd>

<dt>
<label for="email">
<span>Email Address</span>
<span class="required-star">*</span>
</label>
</dt>
<dd>
<input type="email" id="email" name="email" value="<?= htmlentities($email) ?>">
</dd>

</dl>

</fieldset>

<fieldset>
<legend><span class="t22px">Special Requirements</span></legend>

<dl>

<dd class="checkbox">
<label for="disabilityRequirement">I have special disability requirements</label>
<?php
$checked = "";
if ($disabilityRequirement === "1") {
    $checked =  " checked";
}
?>
<input type="checkbox" name="disabilityRequirement" id="disabilityRequirement"<?= $checked ?>>
</dd>

<dt>
<label for="disabilityDetail">Details</label>
</dt>
<dd>
<input type="text" id="disabilityDetail" name="disabilityDetail" value="<?= htmlentities($disabilityDetail) ?>">
</dd>

<dd class="checkbox">
<label for="dietaryRequirement">I have special dietary requirements</label>
<?php
$checked = "";
if ($dietaryRequirement === "1") {
    $checked =  " checked";
}
?>
<input type="checkbox" name="dietaryRequirement" id="dietaryRequirement"<?= $checked ?>>
</dd>

</dd>

<dt>
<label for="dietaryDetail">Details</label>
</dt>
<dd>
<input type="text" id="dietaryDetail" name="dietaryDetail" value="<?= htmlentities($dietaryDetail) ?>">
</dd>

</dl>

</fieldset>

<fieldset>
<legend><span class="t22px">Payment Options</span></legend>

<p>Please select the appropriate payment option below. Please note that some courses may only be available to Lancaster University students.</p>

<dl>

<?php
$costsHtml="";
foreach ($costs as $costItem) {
    switch ($costItem['length']) {
        case "0.5":
            $price = $costItem['day1'];
            break;
        case "1":
            $price = $costItem['day1'];
            break;
        case "2":
            $price = $costItem['day2'];
            break;
        case "3":
            $price = $costItem['day3'];
            break;
        case "5":
            $price = $costItem['special'];
            break;
    }
    $costsHtml .= "<option value='{$costItem['costID']}'";
    if ($costItem['costID'] === $cost) {
        $costsHtml .= " selected='selected'";
    }
    $costsHtml .= ">{$costItem['detail']} - £{$price}</option>\n";
}
?>

<label for="cost">
<span>Payment Option</span>
<span class="required-star">*</span>
<dd>
<select name="cost" id="cost">
<option value=""></option>
<?= $costsHtml ?>
</select>
</dd>

</dl>

</fieldset>

<fieldset>
<legend><span class="t22px">Mailing List</span></legend>

<dl>

<dd class="checkbox">
<label for="agreesToMarketing">Please keep me informed of other courses and events run by the Postgraduate Statistics Centre</label>
<?php
$checked = "";
if ($agreesToMarketing === "1") {
    $checked =  " checked";
}
?>
<input type="checkbox" name="agreesToMarketing"<?= $checked ?>>
</dd>

<dt class="dropdown">
<label for="interest">My particular interest is in:</label>
</dt>
<dd>
<select name="interest" id="interest">
<option value=""></option>
<?php
$agreesToMarketingList = array('Mathematics', 'Statistics (General)', 'Medical Statistics', 'Applied Statistics', 'Environmental Statistics', 'Psychology', 'Social Statistics', 'Criminology', 'Statistical Software', 'Spatial Statistics');
$html = "";
foreach ($agreesToMarketingList as $value) {
    $html .= "<option value=\"" . $value ."\"";
    if ($interest === $value) {
        $html .= " selected='selected'";
    }
    $html .= ">" . $value . "</option>\n";
}
echo $html;
?>
</select>
</dd>

<dt class="dropdown">
<label for="heardAboutUs">I heard about this course via:</label>
</dt>
<dd>
<select name="heardAboutUs" id="heardAboutUs">
<option value=""></option>
<?php
$heardAboutUsList=array(
'psc_web'=>'PSC website',
'psc_flyer'=>'PSC flyer at conference',
'rss_newslatter'=>'Royal Statistical society newsletter',
'rss_web'=>'Royal Statistical Society website',
'ncrm_web'=>'NCRM website',
'allstat_email'=>'Allstat email list',
'dept_email'=>'Faculty/dept email',
'supervisor'=>'Supervisor',
'friend'=>'Friend or colleague',
'other'=>'Other, please specify'
);
$html = "";
foreach ($heardAboutUsList as $key => $value) {
    $html .= "<option value=\"" . $key ."\"";
    if ($heardAboutUs === $key) {
        $html .= " selected='selected'";
    }
    $html .= ">" . $value . "</option>\n";
}
echo $html;
?>
</select>
</dd>

<dt>
<label for="heardAboutUsOther">Details</label>
</dt>
<dd>
<input type="text" id="heardAboutUsOther" name="heardAboutUsOther" value="<?= htmlentities($heardAboutUsOther) ?>">
</dd>

</dl>

</fieldset>

<div>
<input type="hidden" name="courseId" value="<?= $course['courseID'] ?>" >
<input value="Register" type="submit">
</div>

</form>

<?php
//end stage = 2
endif;

//completed
if ($stage === 3) :

if ($payment === "confirmed"):
?>

<h1>Registration</h1>
<p>Thank you for updating our records.</p>

<?php
elseif ($payment === "error"):
?>

<h1>Registration</h1>
<p>We're sorry but there was an error with your request.</p>
<p>Please contact <a href="mailto:psc@lancaster.ac.uk">psc@lancaster.ac.uk</a> to confirm receipt of payment.</p>

<?php
elseif ($payment === "regConfirmed"):
?>

<h1>Registration Confirmed</h1>
<p>Thank you for registering for <strong><?= $course['title'] ?></strong></p>
<p>You will now receive an email confirming your registration. If you do not receive this email within 10 minutes please contact <a href="mailto:psc@lancaster.ac.uk">psc@lancaster.ac.uk</a> to confirm. Please also check any junk folders.</p>

<?php
//payment options
if ($course['fmc'] === "1"):
?>

<h2>Payment Options</h2>
<p>If you wish to be invoiced for your fee, contact <a href="mailto:psc@lancaster.ac.uk">psc@lancaster.ac.uk</a> regarding payment.</p>
<p>If you wish to pay via credit / debit card, visit the <a href="http://online-payments.lancaster-university.co.uk/browse/product.asp?compid=1&modid=1&catid=297">Lancaster University Online Store.</a></p>
<p>Please select <strong><?= $course['duration'] ?></strong> x <strong><?= $fundingDetail['detail'] ?></strong>.</p>

<?php
elseif ($course['sew'] === "1"):
?>

<h2>Payment Options</h2>
<p>This course is free.</p>

<?php
else:
?>

<h2>Payment Options</h2>
<p>Our preferred payment method is via credit / debit card through the Lancaster University Online Store.</p>
<p>If you are able to pay by this method please visit your course-specific link below and select:</p>
<p><strong><?= $course['duration'] ?></strong> x <strong><?= $fundingDetail['detail'] ?></strong>.</p>
<p><a class="btn-more" href="<?= htmlentities($destination['link']) ?>">Payment via online store</a></p>
<p>If you are unable to pay via credit / debit card please contact <a href="mailto:psc@lancaster.ac.uk">psc@lancaster.ac.uk</a> to discuss alternative payment options.</p>

<?php
//end payment options
endif;

//end payment
endif;

//end stage 3
endif;
?>