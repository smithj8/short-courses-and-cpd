<?php

require_once "fst/cpd/mysql_read_pdo.php";

$sql="SELECT courseID, title, start_date, end_date, fmc, other, mps FROM maths_courses WHERE live='1' ORDER BY start_date ASC";
$statement = $db->query($sql);
$courses = $statement->fetchAll();

$currentCoursesHtml = "";
$previousCoursesHtml = "";
$previousCoursesArray = array();
$furtherMathsCentreCoursesHtml = "";
$appliedStatisticsCoursesHtml = "";
$medicalAndPharmaceuticalStatisticsCoursesHtml = "";

foreach ($courses as $course) {
    //handle dates
    $now = new dateTime(null, new DateTimezone("Europe/London"));
    $startDate = new dateTime($course['start_date'], new DateTimezone("Europe/London"));
    $endDate = new dateTime($course['end_date'], new DateTimezone("Europe/London"));
    $m1 = $startDate->format('m');
    $m2 = $endDate->format('m');
    if ($m1 != $m2) {
        $startDateStr = $startDate->format('jS M');
    } else {
        $startDateStr = $startDate->format('jS ');
    }
    $endDateStr = $endDate->format('jS M Y');
    if ($startDate != $endDate) {
        $date = $startDateStr . " - " . $endDateStr;
    } else {
        $date = $endDateStr;
    }
    
    //format link
    $link = str_replace(" ", "-", $course['title']);
    
    //produce HTML
    $courseTableRowHtml = "<tr><td width='75%'><a href='{$link}'>{$course['title']}</a></td><td></td><td width='20%'>{$date}</td></tr>\n";
    
    //put in the right group
    //future courses
    $cutOffDate = $endDate->modify('+1 day');
    $yearStartMonth = 9;
    if ($now < $cutOffDate) {
        $currentCoursesHtml .= $courseTableRowHtml;
        if ($course['fmc'] === '1') {
            $furtherMathsCentreCoursesHtml .= $courseTableRowHtml;
        }
        if ($course['other'] === '1') {
            $appliedStatisticsCoursesHtml .= $courseTableRowHtml;
        }
        if ($course['mps'] === '1') {
            $medicalAndPharmaceuticalStatisticsCoursesHtml .= $courseTableRowHtml;
        }
        
    //previous courses
    //we are in autumn term
    //archived courses are for this year
    } elseif ($now->format('m') >= $yearStartMonth) {
        if (($cutOffDate->format('m') >= $yearStartMonth) and ($cutOffDate->format('y') === $now->format('y'))) {
            $previousCoursesArray[] = $courseTableRowHtml;
        }
        
    //previous courses
    //we are not in autumn term
    //archive course may be within the last 12 months
    } else {
        if (($cutOffDate->format('m') >= $yearStartMonth) and ($cutOffDate->modify('+1 year') > $now)) {
            $previousCoursesArray[] = $courseTableRowHtml;
        }
    }
}

//reverse list of previous courses
if (!empty($previousCoursesArray)) {
    $previousCoursesArray = array_reverse($previousCoursesArray);
    foreach ($previousCoursesArray as $html) {
        $previousCoursesHtml .= $html;
    }
}

?>

<h2>All upcoming courses</h2>
<table width="100%">
<?= $currentCoursesHtml ?>
</table>

<h3>Recent courses</h3>
<table width="100%">
<?= $previousCoursesHtml ?>
</table>

<h2>Further Maths Centre courses</h2>
<table width="100%">
<?= $furtherMathsCentreCoursesHtml ?>
</table>

<h2>Applied Statistics courses</h2>
<table width="100%">
<?= $appliedStatisticsCoursesHtml ?>
</table>

<h2>Medical and Pharmaceutical Statistics courses</h2>
<table width="100%">
<?= $medicalAndPharmaceuticalStatisticsCoursesHtml ?>
</table>
