<?php

require_once "fst/cpd/mysql_read_pdo.php";

//single course
if (isset($_GET['course'])) {
        
    $title = str_replace("-", " ", $_GET['course']);

    $sql = "SELECT * FROM maths_courses WHERE title = :title AND live = '1' ORDER BY start_date DESC";
    $statement = $db->prepare($sql);
    $statement->execute(array(':title' => $title));
    $course = $statement->fetch(PDO::FETCH_ASSOC);

    if ($course) {

        //handle dates
        $now = new dateTime(null, new DateTimezone("Europe/London"));
        $startDate = new dateTime($course['start_date'] . $course['start_time'], new DateTimezone("Europe/London"));
        $endDate = new dateTime($course['end_date'] . $course['end_time'], new DateTimezone("Europe/London"));
        $deadline = new dateTime($course['registration_deadline'], new DateTimezone("Europe/London"));
        $m1 = $startDate->format('m');
        $m2 = $endDate->format('m');
        if ($m1 != $m2) {
            $startDateStr = $startDate->format('jS M');
        } else {
            $startDateStr = $startDate->format('jS ');
        }
        $endDateStr = $endDate->format('jS M Y');
        if ($startDate != $endDate) {
            $dateStr = $startDateStr . " - " . $endDateStr;
        } else {
            $dateStr = $endDateStr;
        }

        //description
        $descriptionHtml = "";
        $description = str_replace("\r", "\n", $course['description']);
        $array = explode("\n", $description);
        foreach ($array as $snippet) {
            if (!empty($snippet)) {
                $descriptionHtml .= "<p>{$snippet}</p>\n";
            }
        }

        //d bullets
        $dBulletsHtml = "";
        $array = explode("\n", $course['d_bullets']);
        foreach ($array as $bullet) {
            $dBulletsHtml .= "<li>{$bullet}</li>\n";
        }
        if ($dBulletsHtml) {
            $dBulletsHtml = "<ul>\n{$dBulletsHtml}</ul>\n";
        }

        //l bullets
        $lBulletsHtml = "";
        $array = explode("\n", $course['l_bullets']);
        foreach ($array as $bullet) {
            $lBulletsHtml .= "<li>{$bullet}</li>\n";
        }
        if ($lBulletsHtml) {
            $lBulletsHtml = "<ul>\n{$lBulletsHtml}</ul>\n";
        }

        //get costs associated with this course
        $sql="SELECT maths_courses.title, maths_costs.detail, maths_costs.day1, maths_costs.day2, maths_costs.day3, maths_costs.special, maths_courses_have_costs.length FROM maths_courses INNER JOIN (maths_costs INNER JOIN maths_courses_have_costs ON maths_costs.costID = maths_courses_have_costs.costID) ON maths_courses.courseID = maths_courses_have_costs.courseID WHERE maths_courses.courseID={$course['courseID']}";
        $statement = $db->prepare($sql);
        $statement->execute();
        $costs = $statement->fetchAll(PDO::FETCH_ASSOC);
        $costsHtml="";
        foreach ($costs as $cost) {
            switch ($cost['length']) {
                case "0.5":
                    $price = $cost['day1'];
                    break;
                case "1":
                    $price = $cost['day1'];
                    break;
                case "2":
                    $price = $cost['day2'];
                    break;
                case "3":
                    $price = $cost['day3'];
                    break;
                case "5":
                    $price = $cost['special'];
                    break;
            }
            $costsHtml .= "<li>{$cost['detail']} - <strong>£{$price}</strong></li>\n";
        }
        if ($costsHtml) {
            $costsHtml = "<ul>\n{$costsHtml}</ul>\n";
        }

        //course header and details
        echo "<h1>" . utf8_encode($course['title']) . "</h1>";
        echo "<p>\n";
        echo "<strong>Date:</strong> {$dateStr}<br>\n";
        echo "<strong>Duration:</strong> ";
        switch ($course['duration']) {
            case "0.5":
                echo "Half a day";
                break;
            case "1":
                echo "1 day";
                break;
            default:
                echo "{$course['duration']} days";
        }
        if ($course['duration_free']) {
            echo " ({$course['duration_free']})<br>\n";
        } else {
            echo "<br>\n";
        }
        if ($course['leader']) {
            echo "<strong>Delivered by:</strong> {$course['leader']}<br>\n";
        }
        if ($course['sponsor']) {
            echo "<strong>Sponsor:</strong> {$course['sponsor']}<br>\n";
        }
        if ($startDate->format('H:i') !== "00:00") {
            echo "<strong>From</strong> {$startDate->format('H:i')} <strong>till</strong> {$endDate->format('H:i')} (each day)<br>\n";
        }
        echo "</p>\n";

        //deadline and booking link
        //deadline hasn't passed
        if ($deadline >= $now) {
            //check attendance
            $sql="SELECT userID FROM maths_users WHERE courseID={$course['courseID']}";
            $statement = $db->prepare($sql);
            $statement->execute();
            if ($course['limit'] >= $statement->rowCount()) {
                echo "<p><strong>Registration deadline</strong>: {$deadline->format('l jS F')}</p>\n";
                echo "<p><a class='btn-more' href='../registration/?courseID={$course['courseID']}'>Book now</a></p>\n";
            } else {
                echo "<p><strong>Registration limit has been reached</strong>.<br>\nPlease contact <a href='mailto:psc@lancaster.ac.uk'>psc@lancaster.ac.uk</a> to get onto a waiting list for this course.<br>\n</p>\n";
            }
        //deadline has passed
        } else {
            echo "<p><strong>Registration deadline has passed</strong>.<br>\nPlease contact <a href='mailto:psc@lancaster.ac.uk'>psc@lancaster.ac.uk</a> for more information about this course.<br>\n</p>\n";
        }

        //costs
        echo "<h3>Cost</h3>";
        if ($course['extra_fees'] === '1') {
            echo "<p><strong>The course fees include all supporting documentation and refreshments.</strong></p>";
        }
        echo $costsHtml;

        //description
        echo "<h3>Course description</h3>\n";
        echo "<p>" . utf8_encode($descriptionHtml) . "</p>\n";

        if ($course['d_bullets']) {
            echo "<h3>" . utf8_encode($course['l_preamble']) . "</h3>\n";
            echo utf8_encode($dBulletsHtml);
        }

        if ($course['learning']) {
            echo "<h3>Learning</h3>\n";
            echo "<p>" . utf8_encode($course['learning']) . "</p>\n";
        }

        if ($course['learning'] and !$course['l_bullets']) {
            echo "<h3>Learning</h3>\n";
        }
        if ($course['d_bullets']) {
            echo "<p><strong>" . utf8_encode($course['l_bullets_preamble']) . "</strong></p>\n";
            echo utf8_encode($lBulletsHtml);
        }

        //different cancellation policy for the statistical education workshops
        if ($course['sew'] === '1') {
            echo "<h3>Cancellation Policy</h3>\n<p>Registrations are transferable to another course or individual at any time. We would be grateful if you could let us know at least 48 hours in advance should you be unable to attend the course.</p>\n";
        } else {
            echo "<h3>Cancellation Policy</h3>\n<p>Registrations are transferable to another course or individual at any time. Full refunds will be given for cancellation 10 or more working days before the course start date. Otherwise the full course fee will be charged.</p>\n";
        }

    //not found in DB
    } else {

        echo "<p>Course not found</p>";

    }

//no slug
} else {

    echo "<p>Course not found</p>";

}

?>
