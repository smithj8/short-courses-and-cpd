<?php

require_once "fst/cpd/mysql_read_pdo.php";

function courseFeesTable($type)
{

    global $db;
    $html = "";

    $tableStartHtml = <<<'HTML'
<table width="100%">
<tr><td width="52%">Participant</td>
<td width="16%">1 Day</td>
<td width="16%">2 Day</td>
<td width="16%">3 Day</td>
</tr>
HTML;

    $tableEndHtml = <<<'HTML'
</table>
HTML;

    //We only want the first six rows from the DB
    $sql = "SELECT detail, day1, day2, day3 FROM maths_costs WHERE {$type} = 1 LIMIT 6";
    $statement = $db->query($sql);
    $result = $statement->fetchAll();

    if ($result) {
        $tableRowsHtml = "";
        foreach ($result as $row) {
            $day1 = is_numeric($row['day1']) ? "£" . trim($row['day1']) : "-" ;
            $day2 = is_numeric($row['day2']) ? "£" . trim($row['day2']) : "-" ;
            $day3 = is_numeric($row['day3']) ? "£" . trim($row['day3']) : "-" ;
            $tableRowsHtml .=  "<tr>\n";
            $tableRowsHtml .=  "<td>{$row['detail']}</td>\n";
            $tableRowsHtml .=  "<td>$day1</td>\n";
            $tableRowsHtml .=  "<td>$day2</td>\n";
            $tableRowsHtml .=  "<td>$day3</td>\n";
            $tableRowsHtml .=  "</tr>\n";
        }

    }

    $html .= $tableStartHtml . $tableRowsHtml . $tableEndHtml;
    return $html;
}
?>

<h2>Medical and Pharmaceutical Statistics Research Unit</h2>
<?= courseFeesTable("mps") ?>

<h2>Applied statistics</h2>
<?= courseFeesTable("other") ?>

<h2>Further Maths Centre</h2>
<?= courseFeesTable("fmc") ?>
